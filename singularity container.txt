Run GitBash (Windows) or open a terminal (Mac) and create and enter a directory to be used with your Vagrant VM.

$ mkdir vm-singularity && cd vm-singularity

If you have already created and used this folder for another VM, you will need to destroy the VM and delete the Vagrantfile.

$ vagrant destroy && rm Vagrantfile

Then issue the following commands to bring up the Virtual Machine. (Substitute a different value for the $VM variable if you like.)

$ export VM=sylabs/singularity-3.0-ubuntu-bionic64 && \
    vagrant init $VM && \
    vagrant up && \
    vagrant ssh


You can check the installed version of Singularity with the following:

vagrant@vagrant:~$ singularity version
3.0.3-1

